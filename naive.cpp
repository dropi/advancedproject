#include<iostream>
#include<fstream>
#include<vector>
#include <conio.h>
#include <ctime>

using namespace std;

int n, m, k;
vector<vector<pair<int, int>>> g;
vector<int> d;

void dfs(int v)
{
    for (int i = 0; i < g[v].size(); ++i)
    {
        int j = g[v][i].first;
        d[j] = min(d[j], d[v] + g[v][i].second);
        dfs(j);
    }
}

int main(int argc, char *argv[])
{
    ifstream in(argv[1]);
    in >> n >> m >> k;
    g.resize(n);
    d.resize(n, 1000000);
    for (int i = 0 ; i < m; ++i)
    {
        int a, b, v;
        in >> a >> b >> v;
        g[a].push_back({b, v});
    }
    clock_t timer = clock();
    d[k] = 0;
    dfs(k);
    timer = clock() - timer;
    for (int i = 0; i < ; ++i)
    {
        cout << i << ": ";
        if (d[i] == 1000000) cout << '-';
        else cout << d[i];
        cout << '\n';
    }

    cout << endl << "Done! Time: " << ((float)timer) / CLOCKS_PER_SEC << 's';
    getch();
}
