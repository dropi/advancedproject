﻿#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "Graph.hpp"




int main()
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    assert(font.loadFromFile("f.ttf"));
    sf::Font ruFont;
    assert(ruFont.loadFromFile("ru.ttf"));
    sf::RenderWindow window(sf::VideoMode(1000, 640), "ShortestPath", sf::Style::Default, settings);
    sf::Text caption;
    caption.setFont(ruFont);
    caption.setFillColor(sf::Color::White);
    caption.setPosition({17*CIRCLE_RADIUS, 0});
    description.setFont(ruFont);
    description.setFillColor(sf::Color::White);
    description.setPosition({0, 575});
    description.setCharacterSize(17);

    start:
    stop_all = false;
    sort_done = false;
    autoPlay = false;
    sortedNodes.clear();
    distances.clear();

    while(!stop_all)
    {

    Graph g;

    g.AddNode({400, 240});
    g.AddNode({500, 340});
    g.AddNode({420, 420});
    g.AddNode({600, 440});
    g.AddNode({580, 260});

    g.ConnectNodes(0, 1);
    g.ConnectNodes(1, 2);
    g.ConnectNodes(2, 3);
    g.ConnectNodes(1, 3);
    g.ConnectNodes(0, 4);
    g.ConnectNodes(4, 3);

    g.setNodeHighlit(0, true);

    int isDragging = -1, isDraggingRight = -1;
    std::pair<int, int> isTyping = {-1, -1};
    int typedNumber = -1;
    bool editMode = true, sort_done_ = false;
    int scrollOffset = 0;

    std::thread th(Search, std::ref(g));

    caption.setString(L"Шаг 0: Задание графа");
    description.setString(L"Управление: [ЛКМ] - Добавить/Переместить вершину, изменить вес ребра; \n[ПКМ] - добавить ребро, удалить ребро/вершину; [Space] - начало/шаг алгоритма; [Enter] - шаг с задержкой;\n[Колесо мыши] - выбор начальной вершины; [R] - начать заново.");

    while (window.isOpen())
    {
        sf::Vector2f mousePosition = {sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y};
        int clickedNode = g.FindNode(mousePosition);
        std::pair<int, int> clickedArrow;
        sf::Event event;
        while(window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                stop_all = true;
                cond_var.notify_all();
                window.close();

            }

            else if (event.type == sf::Event::MouseButtonPressed)
            {
                clickedArrow = g.FindArrow(mousePosition);
                isTyping = {-1, -1};
                typedNumber = -1;
                if(event.mouseButton.button == sf::Mouse::Left)
                {
                    if (clickedNode >= 0)isDragging = clickedNode;
                    else if(clickedArrow.first >= 0 && editMode)
                    {
                        isTyping = clickedArrow;
                        typedNumber = g.GetLength(clickedArrow);
                    }
                    else if(editMode)isDragging = g.AddNode(mousePosition);
                }

                else if (event.mouseButton.button == sf::Mouse::Right && editMode)
                {
                    if (clickedNode >= 0)isDraggingRight = clickedNode;
                    else
                    {
                        g.DeleteArrow(clickedArrow);

                    }
                }

            }

            else if (event.type == sf::Event::MouseButtonReleased)
            {
                if(event.mouseButton.button == sf::Mouse::Left) isDragging = -1;
                else if(event.mouseButton.button == sf::Mouse::Right && editMode)
                {
                    if (isDraggingRight == clickedNode)
                    {
                        g.setNodeHighlit(scrollOffset % g.getGraph().size(), false);
                        g.DeleteNode(clickedNode);
                        g.setNodeHighlit(scrollOffset % g.getGraph().size(), true);
                    }
                    else g.ConnectNodes(isDraggingRight, clickedNode);
                    isDraggingRight = -1;
                }
            }
            else if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Space)
                {
                    if (editMode)
                    {
                        caption.setString(L"Шаг 1: Top sort");
                        editMode = false;
                        startingNode = scrollOffset % g.getGraph().size();
                        scrollOffset = 0;
                    }
                    cond_var.notify_all();
                }
                else if (event.key.code == sf::Keyboard::Return)
                {
                    if (!editMode)
                    {
                        autoPlay = !autoPlay;
                        cond_var.notify_all();
                    }
                    else
                    {
                        isTyping = {-1, -1};
                        typedNumber = -1;
                    }
                }
                else if (event.key.code >= sf::Keyboard::Num0 && event.key.code <= sf::Keyboard::Num9)
                {
                    if (isTyping.first >= 0)
                    {
                        if (typedNumber <= 0) typedNumber = event.key.code - sf::Keyboard::Num0;
                        else if(typedNumber < 99) typedNumber = typedNumber * 10 + (event.key.code - sf::Keyboard::Num0);
                        g.SetLength(isTyping, typedNumber);
                    }
                }
                else if (event.key.code == sf::Keyboard::BackSpace)
                {
                    if (isTyping.first >= 0 && typedNumber >= 0)
                    {
                        typedNumber /= 10;
                        g.SetLength(isTyping, typedNumber);
                    }
                }
                else if (event.key.code == sf::Keyboard::R)
                {
                    stop_all = true;
                    cond_var.notify_all();
                    if (th.joinable()) th.join();
                    goto start;
                }


            }
            else if (event.type == sf::Event::MouseWheelScrolled)
            {
                if (editMode) g.setNodeHighlit(scrollOffset % g.getGraph().size(), false);
                scrollOffset -= event.mouseWheelScroll.delta;
                if (scrollOffset < 0) scrollOffset = 0;
                if (editMode) g.setNodeHighlit(scrollOffset % g.getGraph().size(), true);
            }
        }


        if(sort_done && !sort_done_)
        {
            caption.setString(L"Шаг 2: Поиск расстояний");
            sort_done_ = true;
        }

        g.MoveNode(isDragging, mousePosition);
        window.clear();


        window.draw(caption);
        window.draw(description);
        if (isDraggingRight >= 0 && editMode)
        {
            if (clickedNode >= 0) drawArrow(g.GetNodePosition(isDraggingRight), g.GetNodePosition(clickedNode), CIRCLE_RADIUS, -1, window, sf::RenderStates::Default);
            else drawArrow(g.GetNodePosition(isDraggingRight), mousePosition, 0, -1, window, sf::RenderStates::Default);
        }

        if (!editMode)drawNodeArray(scrollOffset, g, window, sf::RenderStates::Default);
        window.draw(g);
        window.display();
    }

    if (th.joinable()) th.join();

    }

    return 0;
}
