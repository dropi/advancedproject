#ifndef GRAPH_HPP_INCLUDED
#define GRAPH_HPP_INCLUDED
#include <vector>
#include <SFML/Graphics.hpp>
#include <cassert>
#include <sstream>
#include <cmath>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <iostream>

const int INFINITY2 = 2147483647;

const float CIRCLE_RADIUS = 18.0;
const float LINE_THICKNESS = 2.0;

const float PI = atan(1) * 4;
const float RADIANS_TO_DEGREES = 180 / PI;

const int ROWS_ON_SCREEN = 12;

sf::Font font;

void drawNode (sf::Vector2f position, int number, sf::RenderTarget& target, sf::RenderStates states, bool highlited = false);
void drawArrow(sf::Vector2f from, sf::Vector2f to, float dist, float number, sf::RenderTarget& target, sf::RenderStates states, bool highlited = false);
float pointToLineDistance(sf::Vector2f point, sf::Vector2f lineBegin, sf::Vector2f lineEnd);



class Graph:public sf::Drawable{
public:
    Graph();
    //Graph(const std::vector<std::vector<int>>&);

    int FindNode(sf::Vector2f) const;
    int AddNode(sf::Vector2f);
    void MoveNode(int, sf::Vector2f);
    void DeleteNode(int);
    void ConnectNodes(int, int);
    sf::Vector2f GetNodePosition(int) const;
    std::pair<int, int> FindArrow(sf::Vector2f) const;
    void DeleteArrow(std::pair<int, int>);
    void setNodeHighlit(int, bool);
    void setArrowHighlit(std::pair<int, int>, bool);
    std::vector<std::vector<std::pair<int, int>>>& getGraph() {return graph_;}
    void SetLength(std::pair<int, int>, int);
    int GetLength(std::pair<int, int>) const;

    virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

    virtual ~Graph(){}
private:
    std::vector<std::vector<std::pair<int, int>>> graph_;
    std::vector<sf::Vector2f> nodePositions_;
    std::vector<bool> highlitedNodes_;
    std::vector<std::vector<bool>> highlitedArrows_;


    friend void dfs(int node, std::vector<bool>& entered, std::vector<bool>& exited,
         std::vector<int>& sortResult, Graph& graph);
    friend void Search(Graph& graph);
    friend void drawNodeArray(int offset, Graph& graph, sf::RenderTarget& target, sf::RenderStates states);

};

//----------------------------------------------------------------

Graph::Graph(){}


int Graph::AddNode(sf::Vector2f position)
{
    graph_.push_back(std::vector<std::pair<int, int>>());
    nodePositions_.push_back(position);
    highlitedNodes_.push_back(false);
    highlitedArrows_.push_back(std::vector<bool>());
    return nodePositions_.size() - 1;

}

int Graph::FindNode(sf::Vector2f position) const
{
    for (int i = 0; i < nodePositions_.size(); ++i)
    {
        sf::Vector2f difference = position - nodePositions_[i];
        float squareDistance = difference.x * difference.x + difference.y * difference.y;
        if (squareDistance <= CIRCLE_RADIUS * CIRCLE_RADIUS * 1.2) return i;
    }
    return -1;
}

void Graph::DeleteNode(int nodeId)
{
    if (graph_.size() == 1) return;
    if (nodeId < 0) return;
    assert(nodeId < graph_.size());
    graph_.erase(graph_.begin() + nodeId);
    assert(nodeId < nodePositions_.size());
    nodePositions_.erase(nodePositions_.begin() + nodeId);
    assert(nodeId < highlitedNodes_.size());
    highlitedNodes_.erase(highlitedNodes_.begin() + nodeId);
    assert(nodeId < highlitedArrows_.size());
    highlitedArrows_.erase(highlitedArrows_.begin() + nodeId);
    for (int j = 0; j < graph_.size(); ++j)
        for (int i = 0; i < graph_[j].size(); ++i)
            if (graph_[j][i].first == nodeId)
                {
                    graph_[j].erase(graph_[j].begin() + i);
                    highlitedArrows_[j].erase(highlitedArrows_[j].begin() + i);
                }
    for (int j = 0; j < graph_.size(); ++j)
        for (int i = 0; i < graph_[j].size(); ++i)
            if (graph_[j][i].first > nodeId) --graph_[j][i].first;
}

void Graph::ConnectNodes(int first, int second)
{
    if (first < 0 || second < 0) return;

    sf::Vector2f delta = nodePositions_[second] - nodePositions_[first];
    float pathLength = sqrt(delta.x * delta.x + delta.y * delta.y) / CIRCLE_RADIUS;

    for (int i = 0; i < graph_[first].size(); ++i)
    {
        if (graph_[first][i].first == second)
        {
            graph_[first][i].second = pathLength;
            return;
        }
    }
    for (int i = 0; i < graph_[second].size(); ++i)
    {
        if (graph_[second][i].first == first)
        {
            return;
        }
    }

    graph_[first].push_back({second, pathLength});
    highlitedArrows_[first].push_back(false);
}

void Graph::MoveNode(int nodeId, sf::Vector2f position)
{
    if (nodeId < 0) return;
    assert(nodeId < graph_.size());
    nodePositions_[nodeId]= position;
}

sf::Vector2f Graph::GetNodePosition(int nodeId) const
{
    assert(nodeId >= 0);
    assert(nodeId < nodePositions_.size());
    return nodePositions_[nodeId];
}

std::pair<int, int> Graph::FindArrow(sf::Vector2f positon) const
{
    for (size_t i = 0; i < graph_.size(); ++i)
    {
        for (auto line: graph_[i])
        {
            if (positon.x > std::max(nodePositions_[line.first].x, nodePositions_[i].x)) continue;
            if (positon.y > std::max(nodePositions_[line.first].y, nodePositions_[i].y)) continue;
            if (positon.x < std::min(nodePositions_[line.first].x, nodePositions_[i].x)) continue;
            if (positon.y < std::min(nodePositions_[line.first].y, nodePositions_[i].y)) continue;
            float dist = pointToLineDistance(positon, nodePositions_[i], nodePositions_[line.first]);
            if (dist <= LINE_THICKNESS * 1.25 && dist >= -LINE_THICKNESS * 1.25) return {i, line.first};
        }
    }
    return {-1, -1};
}

void Graph::DeleteArrow(std::pair<int, int> arrowEnds)
{
    if (arrowEnds.first < 0 || arrowEnds.second < 0) return;
    for (int i = 0; i < graph_[arrowEnds.first].size(); ++i)
    {
        if (graph_[arrowEnds.first][i].first == arrowEnds.second)
        {
            graph_[arrowEnds.first].erase(graph_[arrowEnds.first].begin() + i);
            highlitedArrows_[arrowEnds.first].erase(highlitedArrows_[arrowEnds.first].begin() + i);
        }
    }
}

void Graph::setNodeHighlit(int nodeId, bool val)
{
    if (nodeId < 0) return;
    assert(nodeId < highlitedNodes_.size());
    highlitedNodes_[nodeId] = val;
}

void Graph::setArrowHighlit(std::pair<int, int> ends, bool val)
{
    if (ends.first < 0 || ends.second < 0) return;

    for (int i = 0; i < graph_[ends.first].size(); ++i)
    {
        if (graph_[ends.first][i].first == ends.second)
        {
            highlitedArrows_[ends.first][i] = val;
        }
    }
}

void Graph::draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const
{
    int N = graph_.size();



    for (size_t i = 0; i < N; ++i)
    {
        for (size_t j = 0; j < graph_[i].size(); ++j)
        {
            drawArrow(nodePositions_[i], nodePositions_[graph_[i][j].first], CIRCLE_RADIUS, graph_[i][j].second, target, states, highlitedArrows_[i][j]);
        }
    }


    for (size_t i = 0; i < N; ++i)
    {
        drawNode(nodePositions_[i], i, target, states, highlitedNodes_[i]);
    }
}

void Graph::SetLength(std::pair<int, int> ends, int val)
{
    if (ends.first < 0 || ends.second < 0) return;

    for (int i = 0; i < graph_[ends.first].size(); ++i)
    {
        if (graph_[ends.first][i].first == ends.second)
        {
            graph_[ends.first][i].second = val;
        }
    }
}

int Graph::GetLength(std::pair<int, int> ends) const
{
    for (int i = 0; i < graph_[ends.first].size(); ++i)
    {
        if (graph_[ends.first][i].first == ends.second)
        {
            return graph_[ends.first][i].second;
        }
    }
    return -1;
}

void drawNode (sf::Vector2f position, int number, sf::RenderTarget& target, sf::RenderStates states, bool highlited)
{
    sf::CircleShape nodeCircle(CIRCLE_RADIUS, 100);
    nodeCircle.setFillColor(sf::Color::Black);
    if (highlited) nodeCircle.setOutlineColor(sf::Color::Yellow);
    else nodeCircle.setOutlineColor(sf::Color::White);
    nodeCircle.setOutlineThickness(LINE_THICKNESS);
    nodeCircle.setOrigin({CIRCLE_RADIUS, CIRCLE_RADIUS});
    nodeCircle.setPosition(position);
    target.draw(nodeCircle, states);


    sf::Text nodeNumber;
    nodeNumber.setScale(0.75, 0.75);
    if (highlited) nodeNumber.setColor(sf::Color::Yellow);
    else nodeNumber.setColor(sf::Color::White);
    nodeNumber.setFont(font);


    std::ostringstream o;
    o << number;
    nodeNumber.setString(o.str());


    sf::FloatRect boundingBox = nodeNumber.getGlobalBounds();
    nodeNumber.setOrigin({boundingBox.width / 1.5, boundingBox.height});
    nodeNumber.setPosition(position);
    target.draw(nodeNumber, states);
}

void drawArrow(sf::Vector2f from, sf::Vector2f to, float dist, float number, sf::RenderTarget& target, sf::RenderStates states, bool highlited)
{
    dist += CIRCLE_RADIUS * 0.5;

    sf::RectangleShape lineRect({LINE_THICKNESS * 2, 1});
    lineRect.setOrigin({LINE_THICKNESS, 0});
    if (highlited) lineRect.setFillColor(sf::Color::Yellow);
    else lineRect.setFillColor(sf::Color::White);

    sf::CircleShape arrowHead(CIRCLE_RADIUS / 2, 3);
    arrowHead.setOrigin({CIRCLE_RADIUS / 2, CIRCLE_RADIUS / 2});
    if (highlited)arrowHead.setFillColor(sf::Color::Yellow);
    else arrowHead.setFillColor(sf::Color::White);


    sf::Vector2f diff = from - to;

    float lineLength = sqrt(diff.x * diff.x + diff.y * diff.y);
    lineRect.setPosition(from);
    lineRect.setScale(1, lineLength - dist);
    lineRect.setRotation(atan(-diff.x / diff.y) * RADIANS_TO_DEGREES);


    if (diff.y >= 0) lineRect.setRotation(lineRect.getRotation() + 180);

    float arrowOffset = ((lineLength - dist)/ lineLength);
    arrowHead.setPosition(from - diff * arrowOffset);
    arrowHead.setRotation(lineRect.getRotation() + 180);

    sf::Text arrowLength;
    arrowLength.setScale(0.5, 0.5);
    if (highlited) arrowLength.setColor(sf::Color::Yellow);
    else arrowLength.setColor(sf::Color::White);
    arrowLength.setOutlineColor(sf::Color::Black);
    arrowLength.setOutlineThickness(CIRCLE_RADIUS / 2);
    arrowLength.setFont(font);

    std::ostringstream o;
    if (number >= 0) o << number;
    arrowLength.setString(o.str());
    float half = 0.5;
    sf::FloatRect boundingBox = arrowLength.getGlobalBounds();
    arrowLength.setOrigin({boundingBox.width / 1.5, boundingBox.height});
    arrowLength.setPosition(to + (diff * half));

    target.draw(lineRect, states);
    target.draw(arrowHead, states);
    target.draw(arrowLength, states);
}

float pointToLineDistance(sf::Vector2f point, sf::Vector2f lineBegin, sf::Vector2f lineEnd)
{
    sf::Vector2f delta = lineEnd - lineBegin;
    return (point.x * delta.y - point.y * delta.x - lineBegin.x * lineEnd.y + lineBegin.y * lineEnd.x)/
            sqrt(delta.x * delta.x + delta.y * delta.y);
}

void drawDistance(sf::Vector2f position, int number, sf::RenderTarget& target, sf::RenderStates states)
{
    sf::Text distText;
    distText.setFont(font);
    distText.setColor(sf::Color::White);
    distText.setPosition(position);
    distText.setScale(0.75, 0.75);
    distText.setString("8");
    distText.setOrigin({0, distText.getGlobalBounds().height});



    std::ostringstream o;
    if (number < INFINITY2) o << number;
    else
    {
        o << 8;
        distText.setRotation(90);
        distText.setOrigin({0, distText.getGlobalBounds().height + CIRCLE_RADIUS});
        distText.setScale({0.75, 0.8});
    }
    distText.setString(o.str());

    target.draw(distText, states);
}
//--------


std::recursive_mutex RM;
std::mutex M, M2;
std::condition_variable_any cond_var;
bool stop_all = false, sort_done = false, autoPlay = false;
std::vector<int> sortedNodes;
std::vector<int> distances;
int startingNode;
sf::Text description;

void dfs(int node, std::vector<bool>& entered, std::vector<bool>& exited,
         std::vector<int>& sortResult, Graph& graph)
{

    if (exited[node]) return;
    if (entered[node]) throw 1;
    std::unique_lock<std::recursive_mutex> lock(RM);

    entered[node] = true;
    graph.highlitedNodes_[node] = true;

    if(stop_all) return;
    if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(3));
    else cond_var.wait(lock);

    int attachedNodes = graph.graph_[node].size();
    for (int i = 0; i < attachedNodes; ++i)
    {
        graph.highlitedArrows_[node][i] = true;
        if(stop_all) return;
        if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
        else cond_var.wait(lock);
        dfs(graph.graph_[node][i].first, entered, exited, sortResult, graph);
        graph.highlitedArrows_[node][i] = false;
        if(stop_all) return;
        if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
        else cond_var.wait(lock);
    }

    exited[node] = true;
    graph.highlitedNodes_[node] = false;

    std::unique_lock<std::mutex> lock2(M2);
    sortResult.push_back(node);
    lock2.unlock();
}

void Search(Graph& graph)
{
    std::unique_lock<std::mutex> lock(M);
    cond_var.wait(lock);
    if (stop_all) return;
    int nodes = graph.graph_.size();

    description.setString(L"\n На этом шаге мы обходим граф при помощи поиска в глубину.\n При выходе из вершины мы добавляем ее в стек.");

    std::vector<bool> entered(nodes, false);
    std::vector<bool> exited(nodes, false);
    try
    {
        dfs(startingNode, entered, exited, sortedNodes, graph);
    }
    catch (int e)
    {
        description.setString(L"\n Найден цикл, продолжать выполнение нет смысла.\n Нажмите [R] чтобы начать сначала.");
        return;
    }


    std::unique_lock<std::mutex> lock2(M2);
    distances.resize(nodes, INFINITY2);
    distances[startingNode] = 0;
    description.setString(L"\n Теперь обходим вершины, находящиеся в стеке в обратном порядке и считаем расстояния до всех вершин,\n в которые идет ребро из текущей.");
    lock2.unlock();
    sort_done = true;

    if(stop_all) return;
    if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
    else cond_var.wait(lock);

    for (int i = sortedNodes.size() - 1; i >= 0; --i)
    {
        graph.highlitedNodes_[sortedNodes[i]] = true;
        if(stop_all) return;
        if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
        else cond_var.wait(lock);
        auto& currentNode = graph.graph_[sortedNodes[i]];
        for (int j = 0; j < currentNode.size(); ++j)
        {
            graph.highlitedArrows_[sortedNodes[i]][j] = true;
            if(stop_all) return;
            if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
            else cond_var.wait(lock);
            lock2.lock();
            distances[currentNode[j].first] =
            std::min(distances[currentNode[j].first], distances[sortedNodes[i]] + currentNode[j].second);
            lock2.unlock();
            if(stop_all) return;
            if(autoPlay)cond_var.wait_for(lock, std::chrono::seconds(1));
            else cond_var.wait(lock);
            graph.highlitedArrows_[sortedNodes[i]][j] = false;
        }
        graph.highlitedNodes_[sortedNodes[i]] = false;
    }
    description.setString(L"\n Работа программы завершена, расстояния найдены.\n Нажмите [R] чтобы начать сначала.");
}

void drawNodeArray(int offset, Graph& graph, sf::RenderTarget& target, sf::RenderStates states)
{
    std::unique_lock<std::mutex> lock2(M2);
    if (offset >= (int)(sortedNodes.size()))offset = (int)(sortedNodes.size()) - 1;
    if (offset < 0) offset = 0;
    int N = std::min((int)(sortedNodes.size() - offset), ROWS_ON_SCREEN);
    for (int i = 0; i < N; ++i)
    {
    bool highlit = (sort_done && graph.highlitedNodes_[sortedNodes[i + offset]]) || (!sort_done && i + 1 == N);
    drawNode({CIRCLE_RADIUS * 2, CIRCLE_RADIUS * (i + 1) * 2.5}, sortedNodes[i + offset], target, states, highlit);
    if (sort_done)drawDistance({CIRCLE_RADIUS * 4, CIRCLE_RADIUS * (i + 1) * 2.5}, distances[sortedNodes[i + offset]], target, states);
    }
    lock2.unlock();
}

#endif // GRAPH_HPP_INCLUDED
