#include <iostream>
#include <fstream>
#include <ios>
#include <vector>
#include <climits>
#include <conio.h>
#include <ctime>

void dfs(std::vector<int>& recursionStack,
         std::vector<bool>& entered, std::vector<bool>& exited,
         std::vector<int>& sortResult,
         std::vector<std::vector<std::pair<int, int>>>& graph)
{

    while (!recursionStack.empty())
    {
        int node = recursionStack.back();
        recursionStack.pop_back();

        if (node >= 0)
        {
            if (entered[node] && !exited[node]) throw 1;

            entered[node] = true;
            recursionStack.push_back(-node - 1);

            int attachedNodes = graph[node].size();
            for (int i = 0; i < attachedNodes; ++i)
            {
                int nextNode = graph[node][i].first;
                if (!exited[nextNode])
                {
                    recursionStack.push_back(nextNode);
                }
            }
        }
        else
        {
            exited[-node - 1] = true;
            sortResult.push_back(-node - 1);
        }

    }
}

///-------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    std::istream* in = &std::cin;
    std::ostream* out = &std::cout;
    std::ifstream fin;
    std::ofstream fout;

    if (argc > 1)
    {
        fin.open(argv[1]);
        if (fin.is_open()) in = &fin;
        else std::cout << "Failed to open file:" << argv[1] << " Switching to console input." << std::endl;
        if (argc > 2)
        {
            fout.open(argv[2]);
            if (fout.is_open()) out = &fout;
            else std::cout << "Failed to open file:" << argv[2] << " Switching to console output." << std::endl;
        }
    }

    int nodes, edges, startingNode;

    try
    {
        (*in) >> nodes;// >> edges >> startingNode;
        if (!(*in))
        {
            std::cout << "Not enough data";
            getch();
            return 0;
        }
        (*in) >> edges;
        if (!(*in))
        {
            std::cout << "Not enough data";
            getch();
            return 0;
        }
        (*in) >> startingNode;
    }
    catch(int rak)
    {
        std::cout << "Not a valid number";
        getch();
        return 0;
    }

    if (nodes <= 0)
    {
        std::cout << "Negative number of nodes";
        getch();
        return 0;
    }
    if (edges < 0)
    {
        std::cout << "Negative number of edges";
        getch();
        return 0;
    }
    if (startingNode < 0)
    {
        std::cout << "Negative starting node";
        getch();
        return 0;
    }
    if (startingNode >= nodes)
    {
        std::cout << "Starting node doesn't exist";
        getch();
        return 0;
    }

    std::vector<std::vector<std::pair<int, int>>> graph(nodes);

    for (int i = 0; i < edges; ++i)
    {
        if (!(*in))
        {
            std::cout << "Not enough data";
            getch();
            return 0;
        }
        int from, to, weight;
        try
        {
            (*in) >> from >> to >> weight;
             if (!(*in))
            {
            std::cout << "Not enough data";
            getch();
            return 0;
            }
        }
        catch (int rak)
        {
            std::cout << "Not a valid number";
            getch();
            return 0;
        }

        if (from < 0 || from >= nodes)
        {
            std::cout << "Node " << from << " doesn't exist";
            getch();
            return 0;
        }
        if (to < 0 || to >= nodes)
        {
            std::cout << "Node " << to << " doesn't exist";
            getch();
            return 0;
        }
        graph[from].push_back({to, weight});
    }

    clock_t timer = clock();

    std::vector<int> sortedNodes;
    std::vector<bool> entered(nodes, false);
    std::vector<bool> exited(nodes, false);

    std::vector<int> recursionStack;
    recursionStack.push_back(startingNode);
    try
    {
        dfs(recursionStack, entered, exited, sortedNodes, graph);
    }
    catch (int e)
    {
        std::cout << "A loop found!";
        getch();
        return 0;
    }

    std::vector<long long> distances(nodes, LLONG_MAX);
    distances[startingNode] = 0;

    for (int i = sortedNodes.size() - 1; i >= 0; --i)
    {
        auto& currentNode = graph[sortedNodes[i]];
        for (int j = 0; j < currentNode.size(); ++j)
        {
            distances[currentNode[j].first] =
            std::min(distances[currentNode[j].first],
                     distances[sortedNodes[i]] + currentNode[j].second);
        }
    }

    timer = clock() - timer;

    if (nodes <= 10000 || argc > 2)
    for (int i = 0; i < nodes; ++i)
    {
        (*out) << std::endl << i << ": ";
        if (entered[i]) (*out) << distances[i];
        else (*out) << '-';
    }
    else std::cout << "Array is to large, use file output to see results.\n";

    std::cout << std::endl << "Done! Time: " << ((float)timer) / CLOCKS_PER_SEC << 's';
    if (!fout.is_open()) getch();
}
